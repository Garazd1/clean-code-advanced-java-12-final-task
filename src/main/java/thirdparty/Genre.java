package thirdparty;

public enum Genre {
    THRILLER,
    COMEDY,
    DRAMA,
    ACTION,
    SCIFI,
    FANTASY,
    HORROR,
    WESTERN,
    ROMANCE
}

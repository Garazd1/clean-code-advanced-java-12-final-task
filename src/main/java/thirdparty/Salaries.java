package thirdparty;

public enum Salaries {

    CAMERA_MAN(6000L),
    ACTOR(50000L),
    SUPERSTAR(100000L),
    RECRUITER(2000L),
    ACCOUNTANT(2500L);

    public final Long proposedSalary;

    Salaries(Long proposedSalary) {
        this.proposedSalary = proposedSalary;
    }

}

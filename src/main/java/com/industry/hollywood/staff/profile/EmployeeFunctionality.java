package com.industry.hollywood.staff.profile;

import com.industry.hollywood.staff.Recruiter.NoSuchProfession;
import thirdparty.service.BudgetIsOverException;
import thirdparty.service.FinancialService;
import thirdparty.staff.StudioEmployee;

public interface EmployeeFunctionality {

    // Accountant job
    void pay(StudioEmployee person, FinancialService financialService) throws BudgetIsOverException;

    // Actor job
    boolean act();

    // CameraMan job
    boolean shoot();

    // Recruiter job
    StudioEmployee hire(String name, String personType) throws NoSuchProfession;

}

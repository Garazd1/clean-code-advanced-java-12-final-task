package com.industry.hollywood.staff;

import com.industry.hollywood.staff.profile.EmployeeFunctionality;
import thirdparty.service.FinancialService;
import thirdparty.staff.StudioEmployee;

import java.util.Random;

import static thirdparty.Salaries.ACTOR;

public class Actor extends StudioEmployee implements EmployeeFunctionality {

    private final boolean isSuperStar;

    public Actor(String name, boolean isSuperStar) {
        super(name, ACTOR.proposedSalary);
        this.isSuperStar = isSuperStar;
    }

    public boolean isSuperStar() {
        return isSuperStar;
    }

    @Override
    public void pay(StudioEmployee person, FinancialService financialService) {

    }

    // returns true if this actor plays nicely and there is no need to repeat the scene again
    @Override
    public boolean act() {
        // superstar highly decreases a chance of failure
        boolean generalSuccessChance = new Random().nextDouble() > 0.04;
        boolean superStarSuccessChance = new Random().nextDouble() > 0.01;
        return isSuperStar ? superStarSuccessChance : generalSuccessChance;
    }

    @Override
    public boolean shoot() {
        return true;
    }

    @Override
    public StudioEmployee hire(String name, String personType) {
        return null;
    }
}

package com.industry.hollywood.staff;

import com.industry.hollywood.staff.profile.EmployeeFunctionality;
import thirdparty.service.FinancialService;
import thirdparty.staff.StudioEmployee;

import static thirdparty.Salaries.RECRUITER;

public class Recruiter extends StudioEmployee implements EmployeeFunctionality {

    public Recruiter(String name) {
        super(name, RECRUITER.proposedSalary);
    }

    @Override
    public void pay(StudioEmployee person, FinancialService financialService) {

    }

    @Override
    public boolean act() {
        return false;
    }

    @Override
    public boolean shoot() {
        return false;
    }

    @Override
    public StudioEmployee hire(String name, String personType) throws NoSuchProfession {
        switch (personType.toLowerCase()) {
            case "accountant": {
                return new Accountant(name);
            }
            case "cameraman": {
                return new CameraMan(name);
            }
            case "superstar": {
                return new Actor(name, true);
            }
            case "actor": {
                return new Actor(name, false);
            }
            default:
                throw new NoSuchProfession(personType);
        }
    }

    public static class NoSuchProfession extends Exception {
        public NoSuchProfession(String personType) {
            super(personType);
        }
    }
}

package com.industry.hollywood.staff.team;

import com.industry.hollywood.staff.Actor;
import com.industry.hollywood.staff.CameraMan;

import java.util.ArrayList;
import java.util.List;

public class StudioStaff {

    private List<Actor> actorsCollection;
    private List<CameraMan> cameramanCollection;

    public StudioStaff(Actor[] actorsCollection,
                       CameraMan[] cameramanCollection) {
        this.actorsCollection = new ArrayList<>();
        this.cameramanCollection = new ArrayList<>();
        for (Actor actor : actorsCollection) {
            addInitialPersonDefinition(actor);
        }
        for (CameraMan cameraMan : cameramanCollection) {
            addInitialPersonDefinition(cameraMan);
        }
    }

    public void addInitialPersonDefinition(CameraMan cameraMan) {
        if (cameramanCollection == null)
            cameramanCollection = new ArrayList<>();
        cameramanCollection.add(cameraMan);
    }

    public void addInitialPersonDefinition(Actor actor) {
        if (actorsCollection == null)
            actorsCollection = new ArrayList<>();
        actorsCollection.add(actor);
    }

    public List<Actor> getActorsCollection() {
        return actorsCollection;
    }

    public List<CameraMan> getCameramanCollection() {
        return cameramanCollection;
    }

}

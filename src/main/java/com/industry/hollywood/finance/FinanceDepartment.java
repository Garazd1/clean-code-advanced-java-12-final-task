package com.industry.hollywood.finance;

import com.industry.hollywood.staff.Accountant;
import thirdparty.service.BudgetIsOverException;
import thirdparty.service.FinancialService;
import thirdparty.staff.StudioEmployee;

import java.util.List;

public class FinanceDepartment implements FinancialService {

    private MovieBudget movieBudget;

    @Override
    public void decreaseBudget(Long paidSum) {
        movieBudget.setBudgetMoney(movieBudget.getBudgetMoney() - paidSum);
    }

    @Override
    public void initBudget(Long initialSum) {
        this.movieBudget = new MovieBudget(initialSum);
    }

    @Override
    public void paySalary(List<StudioEmployee> employees) throws BudgetIsOverException {
        Accountant accountant = (Accountant) employees.stream()
                .filter(person -> person instanceof Accountant)
                .findAny()
                .orElse(null);
        if (accountant != null)
            for (StudioEmployee person : employees) {
                accountant.pay(person, this);
            }
    }

    @Override
    public Long getBudget() {
        return movieBudget.getBudgetMoney();
    }

}
